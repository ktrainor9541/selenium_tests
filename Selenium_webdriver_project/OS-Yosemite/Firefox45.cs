﻿namespace Selenium_Test_Project
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using System;

    [TestFixture]
    [Parallelizable]
    public class OS_Yosemite_Firefox_45
    {
        private IWebDriver driver;
        DesiredCapabilities capability;
        WebDriverWait wait;
        BrowserTests BT = new BrowserTests();
        private string baseURL;

        [SetUp]
        public void Init()
        {
            capability = DesiredCapabilities.Firefox();


            capability.SetCapability("browserstack.user", "ypcmedia1");
            capability.SetCapability("browserstack.key", "gFsRJ4LvoqJ7C9JaNgXz");

            capability.SetCapability("browser", "Firefox");
            capability.SetCapability("browser_version", "45.0");
            capability.SetCapability("os", "OS X");
            capability.SetCapability("os_version", "Yosemite");
            capability.SetCapability("resolution", "1024x768");

            driver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
            baseURL = "http://kelly2.ypcdev.com/US/NY/Rochester/Yellow-Pages/";
        }

        //Tests that the search bar works on the home page
        [Test]
        public void Firefox_Search()
        {
            BT.Search(driver, baseURL, wait);
        }

        //Test "pizza " returns 200 results
        [Test]
        public void Firefox_testPizzaResult()
        {
            BT.testPizzaResult(driver, baseURL, wait);
        }

        //Tests that 3 letter words return standalone results and are not just substrings
        [Test]
        public void Firefox_testBox()
        {
            BT.testBox(driver, baseURL, wait);
        }

        //Tests that CAD works and the mm filter has not been tampered with
        [Test]
        public void Firefox_testCAD()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        //Tests that atm works
        [Test]
        public void Firefox_testAtm()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        //Tests that 'Ambulatory works
        [Test]
        public void Firefox_testAmbulatory()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        [Test]
        public void Firefox_testAmbulatoryMM()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        [Test]
        public void Firefox_testJuvenile()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        [Test]
        public void Firefox_testFarming()
        {
            BT.testCAD(driver, baseURL, wait);
        }

        //Tests for synonyms by searching lawyer and checking that attorney is shown in results
        [Test]
        public void Firefox_testSynonym()
        {
            BT.testSynonym(driver, baseURL, wait);
        }

        [TearDown]
        public void MyTestCleanup()
        {
            driver.Quit();
        }
    }
}