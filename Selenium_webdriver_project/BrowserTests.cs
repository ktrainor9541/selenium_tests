﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Text.RegularExpressions;

namespace Selenium_Test_Project
{
    public class BrowserTests
    {
        public void EnterSearch(string term, string location, IWebDriver driver, string baseURL)
        {
            driver.Navigate().GoToUrl(baseURL);

            driver.FindElement(By.Id("ypcMainPageSearchTerm")).Clear();
            driver.FindElement(By.Id("ypcMainPageSearchTerm")).SendKeys(term);

            driver.FindElement(By.Id("ypcMainPageSearchLocation")).Clear();
            driver.FindElement(By.Id("ypcMainPageSearchLocation")).SendKeys(location);

            driver.FindElement(By.ClassName("btn-default-yellow-hero")).Submit();
        }

        //Tests that the search bar works on the home page and that the search term appears on the next page
        public void Search(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("pizza ", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String results = driver.FindElement(By.ClassName("panel-heading")).Text;
            StringAssert.Contains("'pizza ' near Rochester, NY", results);
        }

        //Test "pizza " returns 200 results
        public void testPizzaResult(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("pizza ", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String results = driver.FindElement(By.ClassName("panel-heading")).Text;
            StringAssert.Contains("181 Search Results", results);
        }

        //Tests that 3 letter words return standalone results and are not just substrings
        public void testBox(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("box", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            } else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bbox\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bbox\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bbox\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //Tests the phase 'CAD' which can give different results depending on if the mm filter has been tampered with
        public void testCAD(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("cad", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bcad\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bcad\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bcad\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //Tests the phase 'atm'
        public void testAtm(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("atm", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\batm\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\batm\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\batm\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //Tests the phase 'Ambulatory' and checks for results with the synonym 'emergency'
        public void testAmbulatory(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("ambulatory", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bemergency\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bemergency\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bemergency\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //The phrase 'ambulatory' gives an exact match without the mm filter
        public void testAmbulatoryMM(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("ambulatory", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bambulatory\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bambulatory\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bambulatory\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //The phrase 'juvenile' gives no results with the mm filter
        public void testJuvenile(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("juvenile", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bjuvenile\b") || Regex.IsMatch(business, @"\badolescents\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bjuvenile\b") || Regex.IsMatch(heading, @"\badolescents\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bjuvenile\b") || Regex.IsMatch(pbs, @"\badolescents\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //Tests that 'farming' returns appropriate results
        public void testFarming(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("farming", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String heading = driver.FindElement(By.ClassName("search_result_heading")).Text.ToLower();
            String pbs;
            if (WebElementExtensions.ElementIsPresent(driver, By.ClassName("highlighted-search-terms")))
            {
                pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            }
            else
            {
                pbs = "";
            }
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\bagricultural\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(heading, @"\bagricultural\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\bagricultural\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }

        //Tests for synonyms by searching lawyer and checking that attorney is shown in results
        public void testSynonym(IWebDriver driver, string baseURL, WebDriverWait wait)
        {
            EnterSearch("lawyer", "Rochester, NY", driver, baseURL);

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search-results-left-column")));

            String business = driver.FindElement(By.ClassName("heading-listing")).Text.ToLower();
            String pbs = driver.FindElement(By.ClassName("highlighted-search-terms")).Text.ToLower();
            bool isTrue = false;

            if (Regex.IsMatch(business, @"\battorney\b") || Regex.IsMatch(business, @"\blaw\b"))
            {
                isTrue = true;
            }
            else if (Regex.IsMatch(pbs, @"\battorney\b") || Regex.IsMatch(pbs, @"\blaw\b"))
            {
                isTrue = true;
            }

            Assert.IsTrue(isTrue);
        }
    }

    //Helper Extension
    public static class WebElementExtensions
    {
        public static bool ElementIsPresent(this IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
