﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;

namespace Selenium_Test_Project
{
    [TestFixture]
    [Parallelizable]
    class IOS_Chrome
    {
        //private string baseURL = "http://kelly2.ypcdev.com/US/NY/Rochester/Yellow-Pages/";
        private IWebDriver driver;
        DesiredCapabilities capability;
        WebDriverWait wait;
        BrowserTests BT = new BrowserTests();

        [SetUp]
        public void Init()
        {
            capability = DesiredCapabilities.Chrome();


            capability.SetCapability("browserstack.user", "ypcmedia1");
            capability.SetCapability("browserstack.key", "gFsRJ4LvoqJ7C9JaNgXz");

            capability.SetCapability("browserName", "iPhone");
            capability.SetCapability("platform", "MAC");
            capability.SetCapability("device", "iPhone 5");

            driver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
        }

    }
}
